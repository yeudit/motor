#include "assets.h"
#include <SDL.h>
#include <SDL_image.h>
#include <fstream>
#include <iostream>

Asset::Asset()
{
}

Asset::~Asset()
{
	DestAsset();
}

// Loads asset from archive
// @AssetID ID number for Asset
// @File File to be opened, fed to PhysFS and SDL
int Asset::LoadAsset(int AssetID, const std::string File)
{
	if (FileCheck(File.c_str()) == 1) {
		OpenFile(File.c_str());
		RW = SDL_RWFromMem(Data, Size);
		TempSurface = IMG_Load_RW(RW, 0);
		AssetOpt = SDL_CreateTextureFromSurface(GetRenderer(), TempSurface);
		SDL_FreeRW(RW);
		printf("AssetID: %d\tAsset loaded from compressed file\n", AssetID);
		return 1;
	}
	if (&AssetOpt == NULL) {
		printf("AssetID: %d\tCould not load asset\n", AssetID);
		SDLErrorPrint();
		return -2;
	} else if (&AssetOpt != NULL) {
		printf("AssetID: %d\tAsset: %s loaded\n", AssetID, File.c_str());
		return 2;
	}
	return 0;
}

int Asset::SetClip(int ClipWidth, int ClipHeight)
{
	int RowIndex = 0;
	int Frames = (TempSurface->h / ClipHeight) * (TempSurface->w / ClipWidth);
	printf("Frames: %d\n", Frames);
	SDL_Rect Clips[Frames];
	for (int i = 0; i < Frames; i++) {
		if (i != 0 && i % (TempSurface->w / ClipWidth) == 0)
			RowIndex++;
		Clips[i].x = i % (TempSurface->w / ClipWidth) * ClipWidth;
		Clips[i].y = RowIndex * ClipHeight;
		Clips[i].w = ClipWidth;
		Clips[i].h = ClipHeight;
	}
	return Frames;
}

// Asset dimensions
// @AssetHight @AssetWidth sets height and width
// @AssetXOfst @AssetYOfst sets Asset X and Y offsets on screen
int Asset::SetRects(int AssetWidth, int AssetHeight, int AssetXOfst, int AssetYOfst)
{
	SrcR.x = 0;
	SrcR.y = 0;
	SrcR.h = AssetHeight;
	SrcR.w = AssetWidth;
	DestR.x = AssetXOfst;
	DestR.y = AssetYOfst;
	DestR.h = AssetHeight;
	DestR.w = AssetWidth;
	return 0;
}

void Asset::DrawAsset(std::string ImageManip)
{
	if (ImageManip == "Stretch" || ImageManip == "stretch") {
		SDL_RenderCopy(GetRenderer(), AssetOpt, NULL, NULL);
	} else if (ImageManip == "" || ImageManip == " ") {
		SDL_RenderCopy(GetRenderer(), AssetOpt, NULL, &DestR);
	}
	SDL_RenderPresent(GetRenderer());
}

// Destroys texture created by LoadAsset
void Asset::DestTexture()
{
	if (AssetOpt != NULL) {
		SDL_DestroyTexture(AssetOpt);
	} else if (AssetOpt == NULL) {
		SDLErrorPrint();
	}
}

int Asset::DestSurface()
{
	if (&TempSurface != NULL) {
		SDL_FreeSurface(TempSurface);
		return 1;
	} else if (&TempSurface == NULL) {
		SDLErrorPrint();
		return -1;
	}
	return 0;
}

void Asset::DestAsset()
{
	DestSurface();
	DestTexture();
	CloseFile();
}
