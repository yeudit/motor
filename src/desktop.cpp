#include "desktop.h"
#include <iostream>
#include <string>

#define GAME_NAME "TestGameName"
#define COMP_NAME "TestCompany"

#ifdef __linux__
const char *xdg_Data;
const char *xdg_Config;
const char *userHome;
std::string data;
std::string config;

const char *setConfigDir()
{
	xdg_Config = std::getenv("XDG_CONFIG");
	userHome = std::getenv("HOME");
	if (userHome != NULL) {
		config += userHome;
		if (xdg_Config != NULL) {
			config += xdg_Config;
			config += GAME_NAME;
		} else if (xdg_Config == NULL) {
			config += "/.config/";
			config += GAME_NAME;
		}
	}
	if (config.c_str() != NULL)
		return config.c_str();
	else
		return NULL;
}

const char *setDataDir()
{
	xdg_Data = std::getenv("XDG_DATA_HOME");
	userHome = std::getenv("HOME");
	if (userHome != NULL) {
		data += userHome;
		if (xdg_Data != NULL) {
			data += xdg_Data;
			data += COMP_NAME;
			data += GAME_NAME;
		} else if (xdg_Data == NULL) {
			data += "/.local/share/" COMP_NAME "/" GAME_NAME;
		}
	}
	if (data.c_str() != NULL)
		return data.c_str();
	else
		return NULL;
}
#endif

#ifdef __WIN32__
#include <windows.h>
#endif
