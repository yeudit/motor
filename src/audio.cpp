#include "audio.h"

Audio::Audio()
{
}

Audio::~Audio()
{
	DestAudio();
}

const char *Audio::GetAudioDevName()
{
	int i, count = SDL_GetNumAudioDevices(0);
	for (i = 0; i < count; ++i) {
		printf("Audio device: %d: %s\n", i, SDL_GetAudioDeviceName(i, 0));
		AudioDevName = SDL_GetAudioDeviceName(i, 0);
		return AudioDevName;
	}
	return NULL;
}

int Audio::CreateAudio(int Freq, Uint16 Form, int Channels, int Chunk)
{
	if (SDL_Init(SDL_INIT_AUDIO) == -1) {
		printf("MIXER WARNING: SDL Audio is not initialized\n");
		return -1;
	}
	SDL_zero(get);
	get.freq = Freq;
	get.format = Form;
	get.channels = Channels;
	get.samples = Chunk;
	dev = SDL_OpenAudioDevice(GetAudioDevName(), 0, &get, &got, SDL_AUDIO_ALLOW_FORMAT_CHANGE);
	if (dev == 0) {
		printf("Audio: Could not open audio: %s\n", SDL_GetError());
	}
	if (Mix_OpenAudio(Freq, Form, Channels, Chunk)) {
		printf("Mixer Error\n");
		return -1;
	} else {
		printf("Mixer created: %d, %d, %d, %d\n", Freq, Form, Channels, Chunk);
		return 1;
	}
	return 0;
}

int Audio::DestAudio()
{
	SDL_CloseAudioDevice(dev);
	Mix_CloseAudio();
	Mix_FreeChunk(sample);
	printf("Mixer destroyed\n");
	return 0;
}

int Audio::OpenAudioFile(const std::string AudioFile, int Comp)
{
	if (Comp == 1) {
		if (FileCheck(AudioFile.c_str()) == 1) {
			OpenFile(AudioFile.c_str());
			sample = Mix_LoadWAV_RW(SDL_RWFromMem(Data, Size), 1);
			printf("Compressed audio file: %s opened\n", AudioFile.c_str());
			return 1;
		} else {
			printf("Compressed audio file: %s failed to open\n", AudioFile.c_str());
			return -1;
		}
	}
	if (Comp != 1) {
		sample = Mix_LoadWAV(AudioFile.c_str());
		if (sample == NULL) {
			printf("Audio file: %s failed to open\n", AudioFile.c_str());
			return -2;
		} else {
			printf("Audio file: %s opened\n", AudioFile.c_str());
			return 2;
		}
	}
	return 0;
}
