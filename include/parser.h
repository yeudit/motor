#ifndef PARSER_H
#define PARSER_H

#include <yaml.h>
#include <string>
#include <cassert>

class YML
{
public:
	FILE oFile;
	yaml_parser_t Parser;
	yaml_document_t Doc;
	yaml_node_t *Map, *Root, *Key, *Val, *Node;
	yaml_node_pair_t *Pair;
	yaml_node_item_t *Val_Item;
	yaml_token_t Event;
	const std::string YMLFile;

	YML();
	~YML();
	int SetParseFile(const std::string YMLFile);
};

#endif
