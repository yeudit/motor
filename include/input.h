#ifndef INPUT_H
#define INPUT_H
#include <SDL_events.h>

class Input
{
public:
	SDL_Event MainEvent;
	Input();
	~Input();
	bool PollEvent();
};

#endif
