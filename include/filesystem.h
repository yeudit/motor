#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <physfs.h>
#include <fstream>
#include <iostream>
#include <string>

class Filesystem
{
public:
	const std::string Archive;
	const std::string Dir;
	const std::string FChck;
	char *Data;
	int Priority;
	PHYSFS_sint64 Size;
	PHYSFS_file *FS_File;

public:
	Filesystem();
	~Filesystem();
	int SetArchive(const std::string Archive);
	int FileCheck(const std::string FChck);
	int CloseFile();
	const char *OpenFile(const std::string PhysFile);
};

#endif
