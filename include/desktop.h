#ifndef DESKTOP_H
#define DESKTOP_H

#include <string>

#ifdef __linux__
const char *setDataDir();
const char *setConfigDir();
#endif

#ifdef __WIN32__
const char *setMyDocuments();
#endif

#endif
